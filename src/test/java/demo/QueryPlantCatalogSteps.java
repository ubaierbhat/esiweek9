package demo;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import demo.models.Plant;

public class QueryPlantCatalogSteps {
	IDatabaseTester db = null;
	
	WebDriver user = null;
	    
    @Before
    public void beforeScenario() throws ClassNotFoundException {
    	if (db == null) {
	    	db = new JdbcDatabaseTester("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/demo", "postgres", "");
    	}
    	System.setProperty("webdriver.chrome.driver", "/Users/lgbanuelos/software/chromedriver");
    	user = new ChromeDriver();
    }
    
    @After
    public void afterScenario() throws Exception {
    	Statement st = db.getConnection().getConnection().createStatement();
    	st.executeUpdate("delete from plant_hire_request");
    	st.executeUpdate("delete from plant");
    	st.close();
//    	user.close();
    }

	@Given("^the following plants are currently available for rental$")
	public void the_following_plants_are_currently_available_for_rental(List<Plant> catalog) throws Throwable {		
		String sql = "insert into plant (id, name, description, price) values (?, ?, ?, ?)";
		PreparedStatement ps = db.getConnection().getConnection().prepareStatement(sql);
		
		for (int i = 0; i < catalog.size(); i++) {
			Plant plant = catalog.get(i);
			ps.setInt(1, i + 1);
			ps.setString(2, plant.getName());
			ps.setString(3, plant.getDescription());
			ps.setFloat(4, plant.getPrice());
			ps.addBatch();
		}
		
		ps.executeBatch();
		ps.close();
	}	

	@Given("^I am in the \"(.*?)\" web page$")
	public void i_am_in_the_web_page(String arg1) throws Throwable {
		user.get("http://localhost:8080/phrs/form");
	}

	@Given("^No Plant Hire Request exists in the system$")
	public void no_Plant_Hire_Request_exists_in_the_system() throws Throwable {
	}

	@When("^I query the plant catalog for an \"(.*?)\" available from \"(.*?)\" to \"(.*?)\"$")
	public void i_query_the_plant_catalog_for_an_available_from_to(String arg1, String arg2, String arg3) throws Throwable {
		user.findElement(By.id("PlantNameID")).sendKeys(arg1);
	}

	@Then("^I should have (\\d+) plants being shown$")
	public void i_should_have_plants_being_shown(int arg1) throws Throwable {
	    throw new PendingException();
	}
}
