Feature: Plant Hire Request Creation
  As a site engineer
  So that I start with the construction project
  I want hire all the required machinery

  Background: Site engineer creates a Plant Hire Request
    Given Site Engineer is in the "Create Plant Hire Request" web page
    And Works Engineer is in the "List of Plant Hire Requests" web page
    When SE queries the plant catalog for an "Excavator" available from "22-09-2014" to "24-09-2014"
	And SE selects the plant with description "1.5 Tonne Mini excavator"
	And SE request the creation of the Plant Hire Request
  Scenario: Works engineer accepts Plant Hire Request
    When WE accepts the newly openned PHR
    And SE browses the lists of PHRs
    Then SE should see the same PHR as accepted
  Scenario: Works engineer rejects Plant Hire Request
    When WE rejects the newly openned PHR
    And SE browses the lists of PHRs
    Then SE should see the same PHR as rejected
    