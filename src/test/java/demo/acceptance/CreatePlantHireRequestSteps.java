package demo.acceptance;

import java.sql.Statement;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreatePlantHireRequestSteps {
	IDatabaseTester db = null;
	WebDriver seng = null;
	WebDriver weng = null;
    
    @Before
    public void beforeScenario() throws ClassNotFoundException {
    	if (db == null) {
	    	db = new JdbcDatabaseTester("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/demo", "postgres", "");
    	}

    	System.setProperty("webdriver.chrome.driver", "/Users/lgbanuelos/software/chromedriver");
    	seng = new ChromeDriver();
    	weng = new ChromeDriver();
    }
    
    @After
    public void afterScenario() throws Exception {
    	Statement st = db.getConnection().getConnection().createStatement();
    	st.executeUpdate("delete from plant_hire_request");
//    	seng.close();
//    	weng.close();
    }

    @Given("^Site Engineer is in the \"Create Plant Hire Request\" web page$")
    public void site_Engineer_is_in_the_web_page() throws Throwable {
    	seng.get("http://localhost:8080/#/phrs/create");
    }

    @Given("^Works Engineer is in the \"List of Plant Hire Requests\" web page$")
    public void works_Engineer_is_in_the_web_page() throws Throwable {
    }

    @When("^SE queries the plant catalog for an \"(.*?)\" available from \"(.*?)\" to \"(.*?)\"$")
    public void se_queries_the_plant_catalog_for_an_available_from_to(String arg1, String arg2, String arg3) throws Throwable {
		seng.findElement(By.id("PlantNameID")).sendKeys(arg1);
		seng.findElement(By.id("StartDateID")).sendKeys(arg2);
		seng.findElement(By.id("EndDateID")).sendKeys(arg3);
		seng.findElement(By.id("SubmitButtonID")).click();
    }

    @When("^SE selects the plant with description \"(.*?)\"$")
    public void se_select_the_plant_with_description(String description) throws Throwable {
    	throw new PendingException();
	}

    @When("^SE request the creation of the Plant Hire Request$")
    public void se_create_of_the_Plant_Hire_Request() throws Throwable {
    	throw new PendingException();
    }

    @When("^WE (.*?) the newly openned PHR$")
    public void we_accepts_one_pending_PHR(String decision) throws Throwable {
    	throw new PendingException();
    }

    @When("^SE browses the lists of PHRs$")
    public void se_browses_the_lists_of_PHRs() throws Throwable {
    	throw new PendingException();
    }

    @Then("^SE should see the same PHR as (.*?)$")
    public void se_should_see_the_same_PHR_as_accepted_or_rejected(String decision) throws Throwable {
    	throw new PendingException();
    }
}
