var app = angular.module('project', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
	    .when('/plants', {
	      controller:'PlantsController',
	      templateUrl:'views/plants/list.html'
	    })
	    .otherwise({
	      redirectTo:'/plants'
	    });
	})