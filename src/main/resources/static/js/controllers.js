var app = angular.module('project');

app.controller('PlantsController', function($scope, $http) {
	$scope.plants = [];
	$http.get('/rest/plants').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.plants = data;
	});
	
});