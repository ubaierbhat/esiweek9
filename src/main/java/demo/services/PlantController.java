package demo.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.PlantResource;
import demo.services.rentit.RentalService;

@RestController
@RequestMapping("/rest/plants")
public class PlantController {
	
	@Autowired
	RentalService rentit;
	
	@RequestMapping(method=RequestMethod.GET, value="")
	public List<PlantResource> queryPlantCatalog(@RequestParam(value="name", required=false) String name) {
		System.out.println(">>>> Parameter: " + name);
		
		List<PlantResource> plants = rentit.findAvailablePlants(name, new Date(), new Date());
		
		return plants;
	}
}
