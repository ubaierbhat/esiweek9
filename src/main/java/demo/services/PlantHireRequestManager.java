package demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.models.PHRStatus;
import demo.models.PlantHireRequest;
import demo.models.repositories.PlantHireRequestRepository;
import demo.services.rentit.RentalService;

@Service
public class PlantHireRequestManager {
    @Autowired
    PlantHireRequestRepository phrRepository;
    @Autowired
    RentalService rentitProxy;    

	public PlantHireRequest createPurchaseOrder(Long id) throws PlantNotAvailableException, RestClientException {
		PlantHireRequest phr = phrRepository.findOne(id);

		PlantResource plantRS = new PlantResource();
		plantRS.add(new Link(phr.getPlantRef()));
		
		PurchaseOrderResource po = rentitProxy.createPurchaseOrder(plantRS, phr.getStartDate(), phr.getEndDate());
		
		phr.setPurchaseOrderRef(po.getId().getHref());
		phr.setPrice(po.getCost());
		phrRepository.save(phr);
		return phr;
	}

	public PlantHireRequest createPlantHireRequest(PlantHireRequest phr) {
		phr.setId(null);
		phr.setStatus(PHRStatus.PENDING);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequest approvePlantHireRequest(Long id) {
		PlantHireRequest phr = phrRepository.findOne(id);
		phr.setStatus(PHRStatus.APPROVED);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequest rejectPlantHireRequest(Long id) {
		PlantHireRequest phr = phrRepository.findOne(id);
		phr.setStatus(PHRStatus.REJECTED);
		
		phrRepository.save(phr);
		return phr;
	}

	public PlantHireRequest getPlantHireRequest(Long id) {
		return phrRepository.findOne(id);
	}

	public List<PlantHireRequest> getAllPlantHireRequests() {
		return phrRepository.findAll();
	}
}
