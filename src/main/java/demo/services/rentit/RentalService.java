package demo.services.rentit;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.services.PlantNotAvailableException;

@Service
public class RentalService {
	@Autowired
	RestTemplate restTemplate;
	
	public List<PlantResource> findAvailablePlants(String plantName, Date startDate, Date endDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		PlantResource[] plants = restTemplate.getForObject(
				"http://localhost:9000/rest/plants?name={name}&startDate={start}&endDate={end}", 
				PlantResource[].class, 
				plantName,
				formatter.format(startDate),
				formatter.format(endDate));
		return Arrays.asList(plants);
	}
	
	public PurchaseOrderResource createPurchaseOrder(PlantResource plant, Date startDate, Date endDate) throws RestClientException, PlantNotAvailableException {
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(plant);
		po.setStartDate(startDate);
		po.setEndDate(endDate);
		
		ResponseEntity<PurchaseOrderResource> result = restTemplate.postForEntity("http://rentit.com/rest/pos", po, PurchaseOrderResource.class);
		
		if (result.getStatusCode().equals(HttpStatus.CONFLICT))
			throw new PlantNotAvailableException();
		
		return result.getBody();
	}
}
