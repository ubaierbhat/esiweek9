package demo.integration.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.PlantHireRequestResource;
import demo.integration.dto.PlantHireRequestResourceAssembler;
import demo.models.PlantHireRequest;
import demo.services.PlantHireRequestManager;
import demo.services.PlantNotAvailableException;

@RestController
@RequestMapping("/rest/phrs")
public class PlantHireRequestRestController {
	@Autowired
	PlantHireRequestManager phrManager;
	
	PlantHireRequestResourceAssembler phrAssembler = new PlantHireRequestResourceAssembler();

	@RequestMapping(method=RequestMethod.GET, value="")
	public List<PlantHireRequestResource> getAllPlantHireRequests() throws Exception {
		List<PlantHireRequest> phrs = phrManager.getAllPlantHireRequests();
		return phrAssembler.toResources(phrs);
	}

	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public PlantHireRequestResource getPlantHireRequest(@PathVariable("id") Long id) throws Exception {
		PlantHireRequest phr = phrManager.getPlantHireRequest(id);
		if (phr == null) throw new ResourceNotFoundException();
		return phrAssembler.toResource(phr);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="")
	public ResponseEntity<PlantHireRequestResource> createPlantHireRequest(@RequestBody PlantHireRequestResource phr) throws Exception {
		PlantHireRequest _phr = new PlantHireRequest();
		_phr.setStartDate(phr.getStartDate());
		_phr.setEndDate(phr.getEndDate());
		if (phr.getPlant() != null) {
			_phr.setPlantRef(phr.getPlant().getId().getHref());
			_phr.setPlantDescription(phr.getPlant().getDescription());
		}
		
		PlantHireRequest newPHR = phrManager.createPlantHireRequest(_phr);
		PlantHireRequestResource res = phrAssembler.toResource(newPHR);
	    HttpHeaders headers = new HttpHeaders();
	    headers.setLocation(new URI(res.getId().getHref()));
    	return new ResponseEntity<>(res, headers, HttpStatus.CREATED);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public PlantHireRequestResource approvePlantHireRequest(@PathVariable("id") Long id) throws Exception {
		PlantHireRequest phr = phrManager.approvePlantHireRequest(id);
		return phrAssembler.toResource(phr);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/{id}/update")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public PlantHireRequestResource update(@PathVariable("id") Long id, @RequestBody PlantHireRequestResource phrr) throws Exception {
		return null;
	}

	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	public PlantHireRequestResource rejectPlantHireRequest(@PathVariable("id") Long id) throws Exception {
		PlantHireRequest phr = phrManager.rejectPlantHireRequest(id);
		return phrAssembler.toResource(phr);
	}
    
	// ==============================================================
    // ==============================================================
	// ======    Methods with interaction with RentIt  ...
	// ====== --------------------------------------------------
	// ======    Should be tested with Mockito !
    // ==============================================================
    // ==============================================================
	
	@RequestMapping(method=RequestMethod.POST, value="/{id}/po")
	@ResponseStatus(HttpStatus.CREATED)
	public PlantHireRequestResource createPurchaseOrder(@PathVariable("id") Long id) throws Exception {
		PlantHireRequest phr = phrManager.createPurchaseOrder(id);
		return phrAssembler.toResource(phr);
	}

	@ExceptionHandler(value={ResourceNotFoundException.class})
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleStatusNotFound() {
	}

	@ExceptionHandler(value={PlantNotAvailableException.class})
	@ResponseStatus(HttpStatus.CONFLICT)
	public void handleStatusConflict() {
	}
}
